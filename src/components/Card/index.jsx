import React from 'react';
import { Card, Icon, Grid, Image } from 'semantic-ui-react';

import './index.scss';

function NewsCard(props) {
	const { karma, authorId, score, time, url, title } = props;
	const date = new Date(time * 1000).toLocaleString().split(',')[0];

	return (
		<Grid.Column>
			<Card>
				<Image src="http://placekitten.com/200/" wrapped ui={false} />
				<Card.Content>
					<Card.Header>
						<a href={url}>{title}</a>
					</Card.Header>
					<Card.Meta>
						<span className="date">Added in {date}</span>
					</Card.Meta>
					<Card.Description>By {authorId}</Card.Description>
				</Card.Content>
				<Card.Content extra  className="news-card-container__stats">
					<div title="score">
						<Icon name="star" />
						{score}
					</div>
					<div title="karma">
						<Icon name="user" />
						{karma}
					</div>
				</Card.Content>
			</Card>
		</Grid.Column>
	);
}

export default NewsCard;
