import React, { useState, useEffect } from 'react';
import { Grid, Header } from 'semantic-ui-react';

import request from './api';
import NewsCard from './components/Card';

import 'semantic-ui-css/semantic.min.css';
import './App.scss';

function App() {
	const [data, setSata] = useState([]);

	const getData = async () => {
		const { data } = await request.get('/topstories.json');
		const shuffled = data.sort(() => 0.5 - Math.random());
		const selected = shuffled.slice(0, 10);
		const requestsItems = selected.map((item) => request.get(`/item/${item}.json`));
		const dataItems = await Promise.all(requestsItems);
		const items = dataItems.map((item) => item.data);
		const requestsUsers = items.map((item) => request.get(`user/${item.by}.json`));
		const usersData = await Promise.all(requestsUsers);
		const users = usersData.map((item) => item.data);
		const mergedData = items.map((item) => {
			let user = users.find((user) => user.id === item.by);
			return { ...item, karma: user.karma };
		});
		//Sorting
		const preparedData = mergedData.sort((a, b) => {
			if (a.score < b.score) {
				return -1;
			} else if (a.score > b.score) {
				return 1;
			}
			return 0;
		});

		setSata(preparedData);
	};

	useEffect(() => {
		getData();
	}, []);

	return (
		<div className="app-container">
			<Grid divided="vertically" doubling stackable columns={5}>
				<Grid.Row columns={1}>
					<Grid.Column>
						<Header as="h1" className="app-container__header">
							Test App
						</Header>
					</Grid.Column>
				</Grid.Row>

				<Grid.Row>
					<>
						{data.length
							? data.map((card) => {
									return (
										<NewsCard
											key={card.time}
											title={card.title}
											url={card.url}
											time={card.time}
											score={card.score}
											authorId={card.by}
											karma={card.karma}
										/>
									);
							  })
							: 'Loading...'}
					</>
				</Grid.Row>
			</Grid>
		</div>
	);
}

export default App;
